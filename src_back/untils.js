export function joinHTML(content) {
  return `<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>test</title>
</head>
<style>
    *{
        font-family: "Microsoft YaHei";
    }
</style>
<body>
    ${content}
</body>
</html>`
}

export function convertImagesToBase64(domName) {
  const dom = document.querySelector(domName)
  if(!dom) return
  var regularImages = dom.querySelectorAll("img");
  var canvas = document.createElement('canvas');
  var ctx = canvas.getContext('2d');
  [].forEach.call(regularImages, function (imgElement) {
    // preparing canvas for drawing
    canvas.width = imgElement.width;
    canvas.height = imgElement.height;
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.drawImage(imgElement, 0, 0, imgElement.width, imgElement.height);
    // by default toDataURL() produces png image, but you can also export to jpeg
    // checkout function's documentation for more details
    var dataURL = canvas.toDataURL();
    imgElement.setAttribute('src', dataURL);
  })
  canvas.remove();
}