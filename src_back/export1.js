import React, {Component} from 'react'
import {joinHTML, convertImagesToBase64} from "./untils";
import { saveAs } from 'file-saver';

const htmlDocx = window.htmlDocx

class ExportButton1 extends Component {
  constructor() {
    super()
    this.state = {}
  }

  render() {
    return (
      <div className="btn export-btn"
        onClick={this.export}>html-docx 导出测试</div>
    )
  }

  componentDidMount() {}

  export = () => {
    convertImagesToBase64('main')
    var content = joinHTML(document.querySelector('main').parentNode.innerHTML)
    var converted = htmlDocx.asBlob(content, {orientation: 'portrait'});
    console.log('converted',converted)

    saveAs(converted, 'a.docx');
  }
}

export default ExportButton1