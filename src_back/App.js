import React, {Component} from 'react';
import ExportButton from './export1'

import './App.css';

// ------------------------------------------
import HTMLStr from '../src/htmlStr'
import HtmlToReact from 'html-to-react'
const HtmlToReactParser = HtmlToReact.Parser
// import html_docx from './asset/html-docx'

// ------------------------------------------

class App extends Component {
  constructor() {
    super()
    this.state = {
      html: ''
    }
  }

  render() {
    return (
      <div className="App">
        <div className="btn-container">
          <ExportButton></ExportButton>
        </div>
       <main>
         {this.state.html}
       </main>
      </div>
    );
  }

  componentDidMount() {
    this.setState({
      html: this.process(HTMLStr)
    });
  }

  process(processStr) {
    let htmlToReactParser = new HtmlToReactParser();
    let htmlExpected = '<div><h1>change chart node to this one</h1></div>';

    let isValidNode = function () {
      return true;
    };

    let processNodeDefinitions = new HtmlToReact.ProcessNodeDefinitions(React);

    let processingInstructions = [
      {
        replaceChildren: true,
        shouldProcessNode: function (node) {
          return node.attribs && node.attribs['data-tag'] === 'chart';
          // return node.attribs && node.attribs['data-test'] === 'foo';
        },
        processNode: function (node, children, index) {
          let htmlToReactParser = new HtmlToReactParser();
          return htmlToReactParser.parse(htmlExpected);
        }
      },
      {
        shouldProcessNode: function (node) {
          return true;
        },
        processNode: processNodeDefinitions.processDefaultNode,
      },
    ];

    let reactComponent = htmlToReactParser.parseWithInstructions(
      processStr, isValidNode, processingInstructions);

    return reactComponent
  }
}

export default App;


// let converted = htmlDocx.asBlob(content, {orientation: 'landscape', margins: {top: 720}});
// saveAs(converted, 'test.docx');
