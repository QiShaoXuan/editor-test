# 编辑器问题解决方案

## HTML 字符串解析渲染，特定标签处理

借助 [html-to-react](https://github.com/aknuds1/html-to-react) 工具

可以达到以下目标：

1. 将 HTML 字符串转为 ReactDom 渲染函数渲染在网页上
2. 将 HTML 字符串中的任意标签替换为目标标签

### 注意

1. 仅能替换特殊标记的元素的内容

### 衍生出来的需要考虑的问题，需要一起讨论以下

1. 如何将 HTML 所需的 CSS 携带到网页中（由 style 标签包裹还是写在行内）
2. 如何将图表的配置信息携带到渲染页面中（在标签中以 `data-` 的形式还是单独包裹标签）
3. 图表信息携带的具体内容

## HTML 字符串转 pdf，word，微信公众号

### pdf

[jsPDF](https://github.com/MrRio/jsPDF)

### word

#### [~~1. html-docx-js~~](https://github.com/evidenceprime/html-docx-js)

~~需要配合 [FileSaver](https://github.com/eligrey/FileSaver.js/) 实现保存~~

##### ~~遇到的问题~~

1. ~~无法使用 `import` 引入文件（严格模式下报错）~~
  - ~~在 HTML 中使用 `script` 引入~~
  - ~~查看源码打包方式，重新打包~~

2. ~~无法使用…~~ 
   - ~~maxOS Chrome 下载，pages 打开为空~~
   - ~~win10 Chrome 下载，office 打开，图片不显示（只有占位）~~

#### 2. [docx](https://github.com/dolanmiu/docx)

需要配合 [FileSaver](https://github.com/eligrey/FileSaver.js/) 实现保存

##### 遇到的问题

1. 需要将 html 解析为对应的 JSON

### 微信公众号

可以直接使用 HTML ，不需要解析或其他处理

## 综合

 html-docx-js 因为各种问题暂时放弃，bug 已给作者在 github 上提过了issues。如果有回复再考虑使用。

当前的主要问题还是在于 HTML 转 word 的问题上，需要一到两天的时间来研究一下 docx 的使用。具体为：

1. 多层嵌套标签解析
2. 图片处理
3. 样式携带
4. 大文件转换性能


## 注意点

### 设置边框颜色
table.getCell(2, 2).properties.cellBorder.addTopBorder

### 设置内容
table.getCell(0, 0).addContent(new docx.Paragraph("Hello"))


## docx demo 注释
- 
