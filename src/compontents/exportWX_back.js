import React from 'react'
import ReactHtmlParser, {convertNodeToElement} from 'react-html-parser';
import juice from 'juice'
import CSSStr from '../asset/cssStr'
import * as untils from '../untils'

const ReactDOMServer = require('react-dom/server');


class ExportWx extends React.Component {
  constructor() {
    super()
    this.state = {}
  }

  static defaultPorps = {
    html: ''
  }

  render() {
    return (
      <div id="ExportWx" onClick={this.export}
        className="btn">点击复制为微信公众号模板 HTML</div>
    )
  }

  componentDidMount() {

  }

  export = () => {

    const result = juice(`<style>${CSSStr}</style>${this.props.html}`);

    const exportHTML = this.process(`<div class="editor-content">${result}</div>`)

    untils.copy(exportHTML)
    // console.log(exportHTML)
    alert('复制成功')
  }

  process(htmlStr) {
    const blockElement = ['div', 'p', 'figure']
    const transform = function (node, index) {

      if (node.type === 'tag' && blockElement.includes(node.name)) {
        node.name = 'section';
        return convertNodeToElement(node, index, transform);
      }
      if (node.type === 'style' && node.name === 'style') {
        return null
      }
    }


    const vdom = ReactHtmlParser(htmlStr ? htmlStr : this.props.html, {
      transform
    })

    return ReactDOMServer.renderToStaticMarkup(vdom);

  }
}

export default ExportWx