import React from 'react'
import ClipboardJS from 'clipboard'

class ExportWX extends React.Component {
  constructor() {
    super()
    this.state = {}
  }

  static defaultProps = {
    clipid:''
  }

  render() {
    return (
      <div id="ExportWx"
        className="btn"  data-clipboard-target={this.props.clipid}>点击复制为微信公众号模板</div>
    )
  }

  componentDidMount() {
    const clipboard = new ClipboardJS('.btn');

    clipboard.on('success', function(e) {
      console.info('Action:', e.action);
      console.info('Text:', e.text);
      console.info('Trigger:', e.trigger);

      e.clearSelection();
      alert('复制成功，请直接粘贴至微信公众号编辑器')
    });

    clipboard.on('error', function(e) {
      console.error('Action:', e.action);
      console.error('Trigger:', e.trigger);
    });
  }
}

export default ExportWX