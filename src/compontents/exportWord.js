import React from 'react'

import * as docx from "docx"
import {saveAs} from 'file-saver'
import {Buffer} from 'buffer'
import HTML from 'html-parse-stringify'


import * as untils from '../untils'

class ExportWord extends React.Component {
  constructor() {
    super()
    this.state = {}
  }

  static defaultProps = {
    html: ''
  }


  render() {
    return (
     <div>
       <div className="btn" onClick={this.parseHtml}>parse</div>
       <div id="Export">
         <div className="btn"
           onClick={this.export}>export docx
         </div>
       </div>
     </div>
    )
  }

  parseHtml=()=>{
    console.log(HTML.parse(this.props.html))

    return HTML.parse(this.props.html)
  }

  export = () => {

    // -------------------- try to add ----------------------
    const doc = new docx.Document()
    doc.Header.createParagraph("Header text")
    doc.Footer.createParagraph("Footer text")

    const paragraph = new docx.Paragraph()
    paragraph.right();
    var text = new docx.TextRun("My awesome text here for my university dissertation");
    paragraph.addRun(text);
    doc.addParagraph(paragraph);


    // var paragraph = new docx.Paragraph("Bullet points").bullet()
    // doc.addParagraph(paragraph)
    //
    // var paragraph2 = new docx.Paragraph("Are awesome").bullet()
    // doc.addParagraph(paragraph2)


    // let table = doc.createTable(2, 2)
    //
    // table.getCell(0, 0).addContent(new docx.Paragraph("Hello"))
    // table.getRow(0).mergeCells(0, 1)
    //
    // doc.createParagraph("Another table").heading2()
    //
    // const lastPage = new docx.TextRun("Total Page Number: ").numberOfTotalPages();
    // paragraph.addRun(lastPage);
    // doc.addParagraph(paragraph)
    // const table = doc.createTable(4, 4);
    // console.log(table.getCell(2, 2))

    // console.log(table.getCell(2, 2).properties.cellBorder.addTopBorder(docx.BorderStyle.DASH_DOT_STROKED, 3, "red"))
    
    // table
    //   .getCell(2, 2)
    //   .addContent(new docx.Paragraph("Hello"))
    //   .Borders.addTopBorder(docx.BorderStyle.DASH_DOT_STROKED, 3, "red")
    //   .addBottomBorder(docx.BorderStyle.DOUBLE, 3, "blue")
    //   .addStartBorder(docx.BorderStyle.DOT_DOT_DASH, 3, "green")
    //   .addEndBorder(docx.BorderStyle.DOT_DOT_DASH, 3, "#ff8000");

    // -------------------- END - try to add ----------------------


    // -------------------- try demo ----------------------
    // const paragraph = new docx.Paragraph("Hello World").pageBreak();

    // doc.addParagraph(paragraph);


    // const paragraph = new docx.Paragraph();
    // const link = doc.createHyperlink("http://www.example.com", "Hyperlink");
    //
    // paragraph.addHyperLink(link);
    // doc.addParagraph(paragraph);

    // -------------------- END - try demo ----------------------

    var packer = new docx.Packer()
    packer.toBlob(doc).then((blob) => {
      saveAs(blob, "example.docx")
    })

    // untils.convertImagesToBase64(['../asset/Screenshot-4-1.png']).then((data) => {
    //
    //   const doc = new docx.Document()
    //
    //   var paragraph = new docx.Paragraph("Some cool text here.")
    //   paragraph.addRun(new docx.TextRun("Lorem Ipsum Foo Bar"))
    //   doc.addParagraph(paragraph)
    //
    //   data.forEach((imgData) => {
    //     const image = docx.Media.addImage(doc, Buffer.from(imgData.base64, "base64"), imgData.img.width, imgData.img.height)
    //     doc.addImage(image)
    //   })
    //
    //   var packer = new docx.Packer()
    //   packer.toBlob(doc).then((blob) => {
    //     saveAs(blob, "example.docx")
    //   })
    // })
  }
}

export default ExportWord