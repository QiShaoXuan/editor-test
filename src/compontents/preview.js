import React from 'react'

class Preview extends React.Component {
  constructor() {
    super()
    this.state = {}
  }

  static defaultProps = {
    html:''
  }

  render() {
    return (
      <div id="Preview" className="preview-container">
        <div className="editor-block-list__layout">
          {this.state.html}
        </div>
      </div>
    )
  }

  componentDidMount(){

  }
}

export default Preview