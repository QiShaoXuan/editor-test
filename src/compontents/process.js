import React, {Component} from 'react'
import ReactHtmlParser, {convertNodeToElement} from 'react-html-parser';

class Process extends Component {
  constructor() {
    super()
    this.state = {
      html: ''
    }
  }

  static defaultPorps = {
    html: ''
  }

  render() {
    return (
      <div className="editor-content">
        <div className="editor-block-list__layout">
          {this.state.html}
        </div>

      </div>
    )
  }

  componentDidMount() {
    this.setState({
      html: this.process()
    });
  }

  process(htmlStr) {
    const blockElement = ['div', 'p', 'ul', 'figure']
    const transform = function (node, index) {
      if (node.type === 'tag' && blockElement.includes(node.name)) {
        node.name = 'section';
        return convertNodeToElement(node, index, transform);
      }
      if (node.type === 'style' && node.name === 'style') {
        return null
      }
    }

    return ReactHtmlParser(htmlStr ? htmlStr : this.props.html, {
      transform
    })

  }
}

export default Process