import React from 'react'
import html2pdf from 'html2pdf.js'

class ExtportPDF extends React.Component {
  constructor() {
    super()
    this.state = {}
  }

  static defaultProps = {
    html: ''
  }


  render() {
    return (
      <div id="ExtportPDF" className='btn'
        onClick={this.exportPDF}>导出为 PDF</div>
    )
  }

  exportPDF = () => {
    const element = document.querySelector('main')
    html2pdf().from(element).save();
  }
}

export default ExtportPDF