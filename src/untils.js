export function joinHTML(content) {
  return `<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>test</title>
</head>
<body>
    ${content}
</body>
</html>`
}

export function convertImagesToBase64(src) {
  const convertTobase64 = function (img) {
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    canvas.width = img.width;
    canvas.height = img.height;
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.drawImage(img, 0, 0, img.width, img.height);
    var dataURL = canvas.toDataURL();
    canvas.remove();

    return dataURL.replace('data:image/png;base64,', '')
  }

  return new Promise((resolve) => {
    Promise.all(src.map((v) => loadImg(v))).then((imgs) => {
      const data = imgs.map((img) => ({
        img: img,
        base64: convertTobase64(img)
      }))
      resolve(data)
    })
  })
}


export function loadImg(src, attr = {}) {
  return new Promise((resolve, reject) => {
    const img = new Image()
    img.src = src

    for (let key in attr) {
      img[key] = attr[key]
    }

    img.onload = function () {
      resolve(img)
    }
  })
}

export function copy(str) {
  const el = document.createElement('textarea')
  el.value = str
  el.setAttribute('readonly', '')
  el.style.position = 'absolute'
  el.style.left = '-9999px'
  el.style.top = '-9999px'
  document.body.appendChild(el)
  const selected = document.getSelection().rangeCount > 0 ? document.getSelection().getRangeAt(0) : false
  el.select()
  document.execCommand('copy')
  document.body.removeChild(el)
  if (selected) {
    document.getSelection().removeAllRanges()
    document.getSelection().addRange(selected)
  }
}