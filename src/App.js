import React, {Component} from 'react';
import Process from './compontents/process'
// import ExportWord from './compontents/exportWord'
import ExportPDF from './compontents/exportPDF'
import ExportWx from './compontents/exportWX'
import ExportWx_back from './compontents/exportWX_back'
import './App.css';
import './asset/gutenberg.css'
// import Preview from './compontents/preview'

import HTMLStr from './htmlStr'



// ------------------------------------------

// import html_docx from './asset/html-docx'

// ------------------------------------------

class App extends Component {
  constructor() {
    super()
    this.state = {
      html: ''
    }
  }

  render() {
    return (
      <div className="App">
        <div className="btn-container">
          <ExportPDF html={HTMLStr}/>
          {/*<ExportWord html={HTMLStr}/>*/}
          <ExportWx_back html={HTMLStr}/>
          <ExportWx  clipid=".editor-content"/>
        </div>
       <main>
         <Process html={HTMLStr}/>
       </main>
      </div>
    );
  }
}

export default App;


// let converted = htmlDocx.asBlob(content, {orientation: 'landscape', margins: {top: 720}});
// saveAs(converted, 'test.docx');
